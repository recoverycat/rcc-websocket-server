const express = require("express")
const WebSocket = require("ws")
const http = require("http")
const uuidv4 = require("uuid/v4")

const app = express()

let port

process.argv.forEach( arg => {
	const match = arg.match(/^port=([^\s]+)/)
	if(match) port = parseInt(match[1])
})

port = port || 9090

// Initialize a http server
const server = http.createServer(app)

// Initialize the WebSocket server instance
const wss = new WebSocket.Server({ server })

let users = {}

const sendTo = (connection, message) => {
	connection.send(JSON.stringify(message))
}


wss.on("connection", ws => {

	// Send immediate response to the incoming connection
	ws.send(
		JSON.stringify({
			type: "connect",
			message: "Well hello there, I am a WebSocket server"
		})
	)

	ws.on("message", msg => {
		let data;
		//accept only JSON messages
		try {
			data = JSON.parse(msg);
		} catch (e) {
			console.log("Invalid JSON");
			data = {};
		}
		const { type, channel, offer, answer} = data;
		switch (type) {
			//when a user tries to login
			case "join":
				users[channel] = users[channel]||[]

				if(!users[channel].includes[ws]) users[channel].push(ws)


				if(channel == "scan-demo"){
					sendTo(ws, {
						type:'data',
						data:'8D0THWf1hSoxzia/Ed3VvoY3lIKLau5Enk4v9YmN9J+KAsKrLmK9Yy1w0I7PIoWTDpZsVjZ5ecguiFQX21omBrS8ejHhw5g2lSCsIACd+Mlvyej4819s1GlOOPJt2on4jJmclOn6Q51s0FZOKzFVsbDi4bYDHYbn3C/tu/H+TACNMxGLC66WUlEHEfkJDbpejsorfN6/VtqlYqnpfniEoNMO7yY1DGgy2ER/HojbkN7uM3G91DCVoZ1fO8xaJFZdMusGwXRFyYf8/OkMV9cnqxl6EnH4Wc9Szl6RLoOcm5svDzqW0VFcHHDks0vIID8C8nJS2n8Cn+SNTdSluadtfHeFTJgdeWr+F2MJCQuwEZrg23e12PsFkUJubPTFkYjyr/7LPenId7pEC2toVLyjqh/znemh/F8z5kvRzwpfN37BK8I+ciimpUhQZoiUqSOWOr95pT38r1RgN48NvjRB1qnBaTrwvfzkYsYYyUuYXG8+eK4B2Jy3iz1+GwL6VYhFtXydPrzHbtizSh5rzArleAg6d2w1c0QV7g6PLvTLI5AmCNtzLVV3JS3NkyFQgyIpowUz20LnsrGCnUNkkhVxFtbrm6qo2HHtPKyOUUb8Sih9ceTaeToHRJxF+QSVysfxYkOSSy+fZst92ohtYztNu7Goh9yYBalaDoSwfAl8JgTxXgV/MkJ6ic5MMIG+H8a09LTW0LM6USudk1SWauamdoezQ/UfGDqXl1McoSvyCAZt6tWE9HuB3VlwydXFrX8G3eIh2q+Aq40DY1C7kScmHBs9Of15xITIOBXAMyO2FDbwlc4YfBuWTkjkMuap7xxIU0sGBim2TOte6si/iFGVvinqM0h+rLi6wkTPHsXnpaekcxxCsjGdcErDvyP8t5tpa+dOa231swERyPhfDOi6zJW7PyX1pJq5IM+dKCCBIgOoRAsxW1KKciH1FApq8kkxiH4D1BLotx0uiqPcdLtIzpo/Nl7An/GnnOeIHnWc/ugWproqrmWZv0/YyQk6ok9e5Do7hwC33JgRIlMrXcHmBFsxukZai1O9iwJN9jcE0KvrJ2Rui3sZsI3+Rllm2FYioWcFmIAIw1QArl6RL/w7z++B1dxHPFTX5tEFiNyTjSJ2QfOyDjovyWRtZcDnxjpqi5q17F7+73BEymmpN2P943ce8CM0eJlZrZitJL5T935cQJ/+d/zKeGiOGLk0leAptRUZkj4HLUmSEY+WaoF+SNoqgaD5HkPX/XbfkIzDsHnDuiSd+aDv/xJeKpS+xcfc46cQHAn1iK20oaghfObf50V9vLS4VgzeyopJFroSgU8uhEWjO/+WCLDz2DqPWNGjXsSGIRpiGwoi0qAZCn+4cH8wrwFdAUBQ9iqwhI3KTm5jTxNYghmj9fnru4pvi710T6/3HxhgZbQ0bdyb1AL200YbjDaSYndoX5oBFyUVbcjm1SfOMABc7CIBwvnOk6i3oXFyJladKP2f6lLMAiND/Eu3lo8f89LnPdGhpw=='
					})
				}


				users[channel].forEach( ws_ => {
					sendTo(ws_, {
						type: "joined",
						self: ws_ == ws,
						channel: channel,
						count: users[channel].length,
						message: "listening in "+channel
					})
				})
			break;
			default:
				Object.values(users)
				.find( channel => channel.includes(ws))
				.forEach(ws_ => {
					if(ws != ws_) sendTo(ws_, data)
				})
			break;
		}
	})

	// Remove current connection from the channel
	ws.on("close", e => {
		for(channel in users){
			users[channel] = users[channel].filter(x => x != ws)
		}
	})


})

// Start the server
server.listen(port, () => {
	console.log(`Websocket Server running on port: ${port}`)
})
