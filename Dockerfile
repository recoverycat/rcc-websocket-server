FROM node:current-alpine

# Create app directory
WORKDIR /usr/src/app

# Install dependencies
COPY package*.json ./
RUN npm ci --only=production

# Copy index.js
COPY index.js .

# Expose port
EXPOSE 9090

CMD ["node","index.js"]
