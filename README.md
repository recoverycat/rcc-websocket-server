# Rcc Websocket Server

This setup needs review.


This service provides a simple websocket server, meant to exchange data between two parties.

## Settings

Uses forver in npm script to let the script run continously:

``` 
npm start 
```

The port on which this service is running is configurable: 

```
forever start -a --uid 'rcc-websocket-server' index.js --port=9999
```

There are no stored states. You can stop and restart the server without harm. 
(All connections will get lost, but they are not meant to be kept longer than a few minutes anyway.)

## Security

There are no special security measurements implemented yet. Treat this service and data sent through it as publically available.
Excrypt data before using this service.